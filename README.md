Chart generation is has been tested using the --dry-run option. Rendering is 
successful at both the top level `madss` chart and at the individual `service-asi` chart.

Both require setting of the global variable `aesKey` which is used to decrypt
encrypted properties in the `values.yaml` files when the yaml of the templates
are being produced. Decryption through Helm rendering does not require any 
additional software to be installated.  An example of how to render the templates
with the aesKey is below:

_NOTE: it is recommended to export the value of the aesKey as an environment_
_variable to minimize exposure in the command line._

_From the root of the repository, run the following:_

`helm install madss ./madss --set global.aesKey=$AES_KEY --dry-run`

_To render only the service-asi chart, run the following:_

`helm install service-asi ./madss/charts/core/charts/service-asi --set global.aesKey=$AES_KEY --dry-run`

Encryption
requires the user to clone [this github repo](https://github.com/chris-parker-za/helm-encryption)
and have python 3.8+ installed on the system being used to encrypt the values.
The repo explains the use of it, but here are the quick points:

- Any key in a values.yaml file beginning with the prefix `encrypted` will be encrypted
  when the `helm-encryption.py --encrypt` command is run.
- Encryption will produce a new `values.enc.yaml` file that should be used to replace 
  the existing `values.yaml` file.
- Decryption will produce a new `values.dec.yaml` file that can be inspected to ensure
  the original values were encrypted properly.

Example commands of using the encryption tool are as follows:

_Encrypt_

`helm-encryption.py --encrypt --path <path to values.yaml> --key $AES_KEY`

_Decrypt_

`helm-encryption.py --decrypt --path <path to values.yaml> --key $AES_KEY`

For convenience, two bash functions have been defined to simplify encryption
and decryption.  They are listed below for convenience and can be made available
by being added to the .bashrc. 

_NOTE: the encryption function automatically replaces the existing values.yaml file_
_with the newly encrypted one. If this is not desired, use the regular command instead_
_or make a backup of the values.yaml file first._

```
helm-encrypt() {
    helm-encryption.py --encrypt --path "$1" --key $AES_KEY && mv "$1"/values.enc.yaml "$1"/values.yaml
}

helm-decrypt() {
    helm-encryption.py --decrypt --path "$1" --key $AES_KEY
}
```